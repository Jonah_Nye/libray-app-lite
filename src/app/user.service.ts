import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http : HttpClient) { }

  signUp(user) : any {
    return this.http.post("http://localhost:3000/users/signUp", user);
  }

  signIn(user) : any {
    this.http.post("http://localhost:3000/users/scanCard", user).subscribe(res => {
      if (res){
        console.log(res);
        this.isSignedIn = true;
      }
    })
  }

  isSignedIn : boolean = false;
}
