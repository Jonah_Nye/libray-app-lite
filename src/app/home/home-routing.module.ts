import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {SearchComponent} from './search/search.component';
import { AddComponent } from './add/add.component';


const routes: Routes = [
  {path: 'home/search', component: SearchComponent},
  {path: 'home/add', component: AddComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
