import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { HomeComponent } from './home.component';
import {SearchComponent} from './search/search.component';
import { Observable } from 'rxjs';

interface Books {  //for search by titlee respinse
  totalBooks: string;
  books: Book[];
}

interface Book {
  author: string;
  available: boolean;
  isbn: number;
  title: string;
  _id: string;
}

@Injectable({
  providedIn: 'root'
})
export class bookService {

  books : Book[];

  constructor(private http : HttpClient) { }

  getBooks () :any{ //half of the time it won't let me use Books, why? Sammie thought it could be an VSC thing, but post-update this still happens. It breaks initial compiling, but not any after
   return this.http.get("http://localhost:3000/books");
  }


  getByTitle(title) : Observable<any> {
    return this.http.get(`http://localhost:3000/books/${title}`)
  }


  addBook(bookToAdd){
    return this.http.post("http://localhost:3000/books/addBook", bookToAdd);
  }

  
}
