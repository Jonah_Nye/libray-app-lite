import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  @Output() titleSearched = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

  title: string;

  search(){
    this.titleSearched.emit(this.title);
  }

}
