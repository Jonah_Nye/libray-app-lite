import { Component, OnInit, Input } from '@angular/core';

interface Book {
  author: string;
  available: boolean;
  isbn: number;
  title: string;
  _id: string;
}

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  //If I had more time I would input an array of properties for this book. A checkout button would toggle the availability status of the book, and output any change using an event emitter back up to search

}
