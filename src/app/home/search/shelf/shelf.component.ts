import { Component, OnInit, Input } from '@angular/core';
import { bookService } from '../../book.service';

@Component({
  selector: 'app-shelf',
  templateUrl: './shelf.component.html',
  styleUrls: ['./shelf.component.css']
})
export class ShelfComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  @Input("books") books;

}
