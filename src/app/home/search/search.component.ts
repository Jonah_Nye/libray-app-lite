import { Component, OnInit } from '@angular/core';
import { bookService } from '../book.service';

interface Books {  //for search by titlee respinse
  totalBooks: string;
  books: Book[];
}

interface Book {
  author: string;
  available: boolean;
  isbn: number;
  title: string;
  _id: string;
}

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  constructor(private bookService : bookService) { }

  ngOnInit() {
    this.bookService.getBooks().subscribe(res => {
      console.log(res);
      this.books = res.books;
    })
  }


  getByTitle(title : string){
    this.bookService.getByTitle(title).subscribe(res => {
      this.books = res.books;
    })
  }

  books : Book[];

}
