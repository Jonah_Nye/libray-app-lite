import { Component, OnInit } from '@angular/core';
import { bookService } from './book.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [bookService]
})
export class HomeComponent implements OnInit {

  constructor(private bookService: bookService) { }

  ngOnInit() {
  }

  author : string;
}
