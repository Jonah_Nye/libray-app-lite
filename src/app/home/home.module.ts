import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { FormComponent } from './search/form/form.component';
import { SearchComponent } from './search/search.component';
import { ShelfComponent } from './search/shelf/shelf.component';
import { BookComponent } from './search/shelf/book/book.component';
import { HttpClientModule } from '@angular/common/http';
import { CardModule, ButtonModule } from 'primeng/primeng';
import { AddComponent } from './add/add.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  imports: [
    CommonModule,
    HomeRoutingModule,
    HttpClientModule,
    CardModule,
    FormsModule,
    ButtonModule
  ],
  declarations: [FormComponent, SearchComponent, ShelfComponent, BookComponent, AddComponent]
})
export class HomeModule { }
