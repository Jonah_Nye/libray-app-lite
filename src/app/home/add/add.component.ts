import { Component, OnInit } from '@angular/core';
import { bookService } from '../book.service';

interface BookToAdd {
title: string;
author: string;
isbn: number
}

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  constructor(private bookService : bookService) { }

  ngOnInit() {
  }

  title : string;
  author : string;
  isbn : number;

  addBook(){

    let bookToAdd : BookToAdd = {
      isbn: this.isbn,
      title: this.title,
      author: this.author
    }

    this.bookService.addBook(bookToAdd).subscribe(res => {
      console.log(res);
    })
  }

}
