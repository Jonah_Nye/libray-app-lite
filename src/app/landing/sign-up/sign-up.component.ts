import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { distinctUntilChanged } from 'rxjs/operators';
import { UserService } from 'src/app/user.service';

interface User {
  email: string;
  password: string;
  fName: string;
  lName: string;
  city: string;
}

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})

export class SignUpComponent implements OnInit {

  //sign up properties
  email: string;
  password: string;
  passwordVerify: string = '';
  fName: string;
  lName: string;
  city: string;

  //property that togles based on password match verification
  pwMatches : boolean;
  
  term = new FormControl();
  
  constructor(private userService : UserService) {
  }

  ngOnInit() {
    this.pwMatches = false;

    this.term.valueChanges.pipe(
      debounceTime(400), 
      distinctUntilChanged()
    )
      .subscribe(result => {
        if (this.passwordVerify.length > 0) { //so blank state doesn't get confirmed
          
          if (result === this.password){
            alert('match');
            return this.pwMatches = true;
          }
          this.pwMatches = false;
        }

      })
  }

  signUp(): void {
    if (this.pwMatches = false){
      return alert('Passwords do not match')
    }

    let user : User = {
      email: this.email,
      password: this.password,
      fName: this.fName,
      lName: this.lName,
      city: this.city
    }
    this.userService.signUp(user).subscribe(res => {
      console.log(res);
      alert(res.message)
    });


  }


}
