import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/user.service';


interface SignIn {
  email: string;
  password: string;
}

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {

  //log in properties
  email : string;
  password : string;

  signIn(){
    let user : SignIn = {
      email: this.email,
      password: this.password
    }

    this.userService.signIn(user);
  }

  constructor(private userService: UserService) { }

  ngOnInit() {
  }

}
