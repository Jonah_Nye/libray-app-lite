import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ButtonModule, CardModule, InputTextModule} from 'primeng/primeng';
import {FormsModule} from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';




import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { LandingComponent } from './landing/landing.component';
import { SignUpComponent } from './landing/sign-up/sign-up.component';
import { SignInComponent } from './landing/sign-in/sign-in.component';
import { UserService } from './user.service';
import { HttpClientModule } from '@angular/common/http';
import { SearchComponent } from './home/search/search.component';
import { HomeComponent } from './home/home.component';
import {HomeModule} from './home/home.module';


@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    SignUpComponent,
    SignInComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule, 
    BrowserAnimationsModule, 
    AppRoutingModule, 
    ButtonModule,
    CardModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    HomeModule,
    InputTextModule
  ],
  providers: [UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
