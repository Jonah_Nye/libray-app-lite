import { NgModule } from '@angular/core';
import { RouterModule, Routes, Router } from '@angular/router';
import { CommonModule } from '@angular/common';
import { LandingComponent } from './landing/landing.component';
import { SearchComponent } from './home/search/search.component';
import { HomeComponent} from './home/home.component';
import { AuthGuard } from './auth.guard';

const routes : Routes = [
  {path: 'landing', component: LandingComponent},
  {path: 'home',  canActivate: [AuthGuard], component: HomeComponent},
  {path: '', component: LandingComponent}
]

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes),
    CommonModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
